{{-- @section('content')

<div class="row">

    <div class="col-lg-12">

        <div class="left-area">

            <h4 class="heading">{{ __('Curier') }}*</h4>

        </div>

    </div>

    <div class="col-lg-12">

        <select id="cat" name="category_id" required="">

            <option value="">{{ __('Select Curier') }}</option>

            @foreach($users as $cat)

            <option data-href="#"

                value="{{ $cat->curier_id }}">{{$cat->curier_partner}}</option>

            @endforeach

        </select>

    </div>

</div>

@endsection --}}
@extends('layouts.vendor')

@section('content')
<html>
  <head>
    <meta charset="utf-8">
    <title>Laravel Dependent Dropdown  Tutorial With Example</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  </head>
  <body>

    <form action="#"  enctype="multipart/form-data">

        {{csrf_field()}}
        {{-- @csrf_field --}}

        {{-- Start Editing Length --}}

        {{-- @csrf --}}
        {{-- @method('PUT') --}}
        {{-- @csrf_field --}}
    <div class="container">
    {{-- <h2>Laravel Dependent Dropdown  Tutorial With Example</h2> --}}
            <div class="form-group">
                <label for="curier">{{ __('First Choice') }}*</label>
                <select name="curier1" class="form-control" style="width:250px">
                    <option value="">{{ __('Select Curier') }}</option>
                    @foreach ($users as $value)
                    <option value="{{ $value->id }}">{{ $value->curier_partner }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="curier">{{ __('Second Choice') }}*</label>
                <select name="curier2" class="form-control" style="width:250px">
                    <option value="">{{ __('Select Curier') }}</option>
                    @foreach ($users as $value)
                    <option value="{{ $value->id }}">{{ $value->curier_partner }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="curier">{{ __('Third Choice') }}*</label>
                <select name="curier3" class="form-control" style="width:250px">
                    <option value="">{{ __('Select Curier') }}</option>
                    @foreach ($users as $value)
                    <option value="{{ $value->id }}">{{ $value->curier_partner }}</option>
                    @endforeach
                </select>
            </div>

      </div>

      <div class="text-center" style="margin-top: 10px;">
        <button type="submit" class="btn btn-success">Save</button>
    </div>
    </form>
  </body>
</html>
{{-- <h2>

</h2>
<h1>Curier</h1> --}}
@endsection

