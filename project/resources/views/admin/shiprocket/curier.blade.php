@extends('layouts.admin')



@section('styles')



<style type="text/css">



.input-field {

    padding: 15px 20px;

}



</style>



@endsection



@section('content')



<input type="hidden" id="headerdata" value="{{ __('Curier') }}">



                    <div class="content-area">

                        <div class="mr-breadcrumb">

                        <div class="product-area">

                            <div class="row">

                                <div class="col-lg-12">

                                    <div class="mr-table allproduct">

                                        @include('includes.admin.form-success')

                                        <div class="table-responsiv">

                                        <div class="gocover" style="background: url({{asset('assets/images/'.$gs->admin_loader)}}) no-repeat scroll center center rgba(45, 45, 45, 0.5);"></div>

                                                <table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">

                                                        <tr>

                                                            <th>{{ __('Curier ID') }}</th>

                                                            <th>{{ __('Curier Name') }}</th>

                                                            <th>{{ __('Pickup Performance') }}</th>

                                                            <th>{{ __('Delivery Performance') }}</th>

                                                            <th>{{ __('NDR Performance') }}</th>

                                                            <th>{{ __('RTO Performance') }}</th>

                                                            <th>{{ __('Weight Cases') }}</th>

                                                            <th>{{ __('Overall Rating') }}</th>

                                                            <th>{{ __('Price') }}</th>

                                                            {{-- <th>{{ __('Delivery Details') }}</th> --}}

                                                            {{-- <th>{{ __('Action ') }}</th> --}}

                                                        </tr>
                                                        @foreach($results['available_courier_companies'] as $key=>$item)
                                                         <tr>
                                                             <td>
                                                                 {{ $item['courier_company_id'] }}
                                                             </td>
                                                             <td>
                                                                {{ $item['courier_name'] }}<br/>
                                                                Min Weight: {{ $item['min_weight'] }}<br/>
                                                                {{-- {{ $item['rto_charges'] }} --}}
                                                            </td>
                                                             <td>
                                                                {{ $item['pickup_performance'] }} <br/>
                                                                Call Before Delivery : {{ $item['call_before_delivery'] }}
                                                            </td>
                                                            <td>
                                                                {{ $item['delivery_performance'] }} <br/>
                                                                POD: {{ $item['pod_available'] }}
                                                            </td>
                                                            <td>
                                                                {{ $item['tracking_performance'] }}<br/>
                                                                Delivery Boy Number: {{ $item['delivery_boy_contact'] }}
                                                            </td>
                                                            <td>
                                                                {{ $item['rto_performance'] }}
                                                            </td>
                                                            <td>
                                                                {{ $item['weight_cases'] }}<br/>
                                                                Tracking Service: {{ $item['realtime_tracking'] }}
                                                            </td>
                                                            <td>
                                                                {{ $item['rating'] }}
                                                            </td>
                                                            <td>
                                                                ₹ {{ $item['rate'] }}<br/>
                                                                Freight Charges: ₹ {{ $item['freight_charge'] }}<br/>
                                                                + COD Charges: ₹ {{ $item['cod_charges'] }}<br/>


                                                            </td>
                                                            {{-- <td>
                                                                Expected Pickup: {{ $item['suppress_date'] }}<br/>
                                                                Estimated Delivery: <br/>
                                                                {{ $item['etd'] }}
                                                            </td> --}}

                                                             {{-- <td>
                                                                <a href="#" class="mybtn1"><i class="fa fa-shopping-cart"></i> {{ __('Ship') }}</a>
                                                                {{-- <button class="btn btn-primary" href="{{ route('get-ship-curier',$item['id']) }}">Ship</button> --}}
                                                            {{-- </td> --}}
                                                         </tr>
                                                        @endforeach

                                                </table>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>



                    </div>






@endsection



@section('scripts')







@endsection
