@extends('layouts.admin')



@section('styles')



<style type="text/css">

    .order-table-wrap table#example2 {

    margin: 10px 20px;

}



</style>



@endsection





@section('content')

    <div class="content-area">

                        <div class="mr-breadcrumb">

                            <div class="row">

                                <div class="col-lg-12">

                                        <h4 class="heading">{{ __('Order Details') }} <a class="add-btn" href="javascript:history.back();"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h4>

                                        <ul class="links">

                                            <li>

                                                <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>

                                            </li>

                                            <li>

                                                <a href="javascript:;">{{ __('Orders') }}</a>

                                            </li>

                                            <li>

                                                <a href="javascript:;">{{ __('Order Details') }}</a>

                                            </li>

                                        </ul>

                                </div>

                            </div>

                        </div>



                        <div class="order-table-wrap">

                            @include('includes.admin.form-both')

                            <div class="row">



                                <div class="col-lg-6">

                                    <div class="special-box">

                                        <div class="heading-area">

                                            <h4 class="title">

                                            {{ __('Order Details') }}

                                            </h4>

                                        </div>

                                        <div class="table-responsive-sm">

                                            <table class="table">

                                                <tbody>

                                                <tr>

                                                    <th class="45%" width="45%">{{ __('Order ID') }}</th>

                                                    <td width="10%">:</td>

                                                    <td class="45%" width="45%">{{$results['id']}}</td>

                                                </tr>

                                                <tr>

                                                    <th width="45%">{{ __('Channel ID') }}</th>

                                                    <td width="10%">:</td>

                                                    <td width="45%">{{$results['channel_id']}}</td>

                                                </tr>

                                                <tr>

                                                    <th width="45%">{{ __('Channel Name') }}</th>

                                                    <td width="10%">:</td>

                                                    <td width="45%">{{$results['channel_name']}}</td>

                                                </tr>

                                                <tr>

                                                    <th width="45%">{{ __('Order Date') }}</th>

                                                    <td width="10%">:</td>

                                                    <td width="45%">{{$results['order_date']}}</td>

                                                </tr>

                                                </tbody>

                                            </table>

                                        </div>

                                        <div class="footer-area">

                                            <a href="{{ route('generate-invoice',$results['id']) }}" class="mybtn1"><i class="fa fa-info"></i> {{ __('Invoice') }}</a>
                                            {{-- <a href="{{ route('api/push-order/{"$order->id"}') }}" class="mybtn1"><i class="fas fa-eye"></i> {{ __('Push Order') }}</a> --}}

                                        </div>


                                    </div>

                                </div>

                                <div class="col-lg-6">

                                    <div class="special-box">

                                        <div class="heading-area">

                                            <h4 class="title">

                                            {{ __('Customer Details') }}

                                            </h4>

                                        </div>

                                        <div class="table-responsive-sm">

                                            <table class="table">

                                                <tbody>

                                                    <tr>

                                                        <th class="45%" width="45%">{{ __('Customer Name') }}</th>

                                                        <td width="10%">:</td>

                                                        <td class="45%" width="45%">{{$results['customer_name']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Customer Email') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$results['customer_email']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Customer Address') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$results['customer_address']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Customer Pincode') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$results['customer_pincode']}}</td>

                                                    </tr>

                                                </tbody>

                                            </table>

                                        </div>

                                    </div>

                                </div>

                                <div class="col-lg-6">

                                    <div class="special-box">

                                        <div class="heading-area">

                                            <h4 class="title">

                                            {{ __('Pickup Details') }}

                                            </h4>

                                        </div>

                                        <div class="table-responsive-sm">

                                            <table class="table">

                                                <tbody>

                                                    <tr>

                                                        <th class="45%" width="45%">{{ __('Pickup Code') }}</th>

                                                        <td width="10%">:</td>

                                                        <td class="45%" width="45%">{{$results['pickup_code']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Paymnet Method') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$results['payment_method']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Pickup Address') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$results['pickup_location']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Currency') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$results['currency']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Delivery Code') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$results['delivery_code']}}</td>

                                                    </tr>

                                                </tbody>

                                            </table>

                                        </div>

                                    </div>

                                </div>

                                <div class="col-lg-6">

                                    <div class="special-box">

                                        <div class="heading-area">

                                            <h4 class="title">

                                            {{ __('Product Details') }}

                                            </h4>

                                        </div>

                                        <div class="table-responsive-sm">

                                            <table class="table">
                                                @foreach($results['products'] as $product)
                                                <tbody>

                                                    <tr>

                                                        <th class="45%" width="45%">{{ __('Product') }}</th>

                                                        <td width="10%">:</td>

                                                        <td class="45%" width="45%">{{$product['name']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Sku') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$product['sku']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Quantity') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$product['quantity']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Selling Priced') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$product['selling_price']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Total') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$product['net_total']}}</td>

                                                    </tr>

                                                </tbody>
                                                @endforeach
                                            </table>

                                        </div>

                                    </div>

                                </div>

                                <div class="col-lg-6">

                                    <div class="special-box">

                                        <div class="heading-area">

                                            <h4 class="title">

                                            {{ __('Shipment Details') }}

                                            </h4>

                                        </div>

                                        <div class="table-responsive-sm">

                                            <table class="table">
                                                {{-- @foreach($results['shipments'] as $shipment) --}}
                                                <tbody>

                                                    <tr>

                                                        <th class="45%" width="45%">{{ __('Shipment ID') }}</th>

                                                        <td width="10%">:</td>

                                                        <td class="45%" width="45%">{{$results['shipments']['id']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Dimentions') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$results['shipments']['dimensions']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Weight') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$results['shipments']['weight']}}</td>

                                                    </tr>

                                                    {{-- <tr>

                                                        <th width="45%">{{ __('Curier') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$results['shipments']['courier']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Manifest Escalate') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$results['shipments']['manifest_escalate']}}</td>

                                                    </tr>

                                                    <tr>

                                                        <th width="45%">{{ __('Shipped Date') }}</th>

                                                        <td width="10%">:</td>

                                                        <td width="45%">{{$results['shipments']['shipped_date']}}</td>

                                                    </tr> --}}

                                                </tbody>
                                                {{-- @endforeach --}}
                                            </table>

                                        </div>

                                    </div>

                                </div>


                        </div>

                    </div>

                    <!-- Main Content Area End -->

                </div>

            </div>


    </div>




@endsection





@section('scripts')



<script type="text/javascript">

$('#example2').dataTable( {

  "ordering": false,

      'lengthChange': false,

      'searching'   : false,

      'ordering'    : false,

      'info'        : false,

      'autoWidth'   : false,

      'responsive'  : true

} );

</script>



    <script type="text/javascript">

        $(document).on('click','#license' , function(e){

            var id = $(this).parent().find('input[type=hidden]').val();

            var key = $(this).parent().parent().find('input[type=hidden]').val();

            $('#key').html(id);

            $('#license-key').val(key);

    });

        $(document).on('click','#license-edit' , function(e){

            $(this).hide();

            $('#edit-license').show();

            $('#license-cancel').show();

        });

        $(document).on('click','#license-cancel' , function(e){

            $(this).hide();

            $('#edit-license').hide();

            $('#license-edit').show();

        });



        $(document).on('submit','#edit-license' , function(e){

            e.preventDefault();

          $('button#license-btn').prop('disabled',true);

              $.ajax({

               method:"POST",

               url:$(this).prop('action'),

               data:new FormData(this),

               dataType:'JSON',

               contentType: false,

               cache: false,

               processData: false,

               success:function(data)

               {

                  if ((data.errors)) {

                    for(var error in data.errors)

                    {

                        $.notify('<li>'+ data.errors[error] +'</li>','error');

                    }

                  }

                  else

                  {

                    $.notify(data,'success');

                    $('button#license-btn').prop('disabled',false);

                    $('#confirm-delete').modal('toggle');



                   }

               }

                });

        });

    </script>



@endsection
