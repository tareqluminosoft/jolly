<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use PhpParser\JsonDecoder;

class Order extends Model
{
	protected $fillable = [
    'user_id',
    'cart',
    'method',
    'shipping',
    'pickup_location',
    'totalQty',
    'pay_amount',
    'txnid',
    'charge_id',
    'order_number',
    'payment_status',
    'customer_email',
    'customer_name',
    'customer_phone',
    'customer_address',
    'customer_city',
    'customer_zip',
    'shipping_name',
    'shipping_email',
    'shipping_phone',
    'shipping_address',
    'shipping_city',
    'shipping_zip',
    'order_note',
    'status'];

    public function vendororders()
    {
        return $this->hasMany('App\Models\VendorOrder');
    }

    public function tracks()
    {
        return $this->hasMany('App\Models\OrderTrack','order_id');
    }

    public static function pushOrder($order_id){
        $orderDetails = Order::where('id', $order_id)->first()->toArray();

        $orderDetails['order_id'] = $orderDetails['id'];
        $orderDetails['order_date'] = $orderDetails['created_at'];
        $orderDetails['pickup_location'] = "LuminoSoft Office";
        $orderDetails['channel_id'] = "2394849";
        $orderDetails['comment'] = "Luminosoft Office Order";
        $orderDetails['reseller_name'] = "";
        $orderDetails['company_name'] = "Luminosoft";
        $orderDetails['billing_customer_name'] = $orderDetails['customer_name'];
        $orderDetails['billing_last_name'] = "";
        $orderDetails['billing_address'] = $orderDetails['customer_address'];
        $orderDetails['billing_address_2'] = "";
        $orderDetails['billing_city'] = $orderDetails['customer_city'];
        $orderDetails['billing_pincode'] = $orderDetails['customer_zip'];
        $orderDetails['billing_state'] = $orderDetails['customer_city'];
        $orderDetails['billing_country'] = $orderDetails['customer_country'];
        $orderDetails['billing_email'] = $orderDetails['customer_email'];
        $orderDetails['billing_phone'] = $orderDetails['customer_phone'];
        $orderDetails['shipping_is_billing'] = true;
        $orderDetails['shipping_customer_name'] = $orderDetails['customer_name'];
        $orderDetails['shipping_last_name'] = "";
        $orderDetails['shipping_address'] = $orderDetails['customer_address'];
        $orderDetails['shipping_address_2'] = "";
        $orderDetails['shipping_city'] = $orderDetails['customer_city'];
        $orderDetails['shipping_pincode'] = $orderDetails['customer_zip'];
        $orderDetails['shipping_state'] = $orderDetails['customer_city'];
        $orderDetails['shipping_country'] = $orderDetails['customer_country'];
        $orderDetails['shipping_email'] = $orderDetails['customer_email'];
        $orderDetails['shipping_phone'] = $orderDetails['customer_phone'];


        $cart = json_decode($orderDetails['cart'],true);

        $orderDetails['order_items'] = $cart['items'];
        foreach($orderDetails['order_items'] as $key=> $items){

            $length  = $items['length'];
            $width = $items['width'];
            $height = $items['height'];
            $weight = $items['weight'];

            // print_r($height);die();

            $orderDetails['order_items'][$key]['name'] = $items['item']['name'];
            $orderDetails['order_items'][$key]['sku'] = $items['item']['id'];
            $orderDetails['order_items'][$key]['units'] = $items['qty'];
            $orderDetails['order_items'][$key]['selling_price'] = $items['price'];
            $orderDetails['order_items'][$key]['discount'] = "";
            $orderDetails['order_items'][$key]['tax'] = "";
            $orderDetails['order_items'][$key]['hsn'] = "";



        }

        // $order_items = $cart['items'];
        $orderDetails['payment_method'] = $orderDetails['method'];
        $orderDetails['shipping_charges'] = $orderDetails['shipping_cost'];
        $orderDetails['giftwrap_charges'] = $orderDetails['packing_cost'];
        $orderDetails['transaction_charges'] = "";
        $orderDetails['total_discount'] = $orderDetails['coupon_discount'];
        $orderDetails['sub_total'] = $orderDetails['pay_amount'];
        $orderDetails['length'] = $length;
        $orderDetails['breadth'] = $width;
        $orderDetails['height'] = $height;
        $orderDetails['weight'] = $weight;

        // echo "<pre>"; print_r(json_encode($orderDetails)); die;

        $orderDetails = json_encode($orderDetails);

        // Get Access Token
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);
        // echo "<prep>"; print_r($server_output); die;

        // Crete Order in Ship Rocket through Code
        $url = "https://apiv2.shiprocket.in/v1/external/orders/create/adhoc";
        $c = curl_init($url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,$orderDetails);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $result = json_decode($result,true);
        // print_r($result);

        if(isset($result['status_code'])&&$result['status_code']==1)
        {
            Order::where('id', $order_id)->update(['order_id'=>$result['order_id'],'shipment_id'=>$result['shipment_id']]);
            // print_r($result);
            return redirect()->route('ship-order-details');
        }
        else
        {
            print_r($result);
            return "There is an Issue when pushing order to shiprocket! Please Contcat with Admin";
        }

        return redirect()->route('ship-order-details');


    }

    public static function updateOrder($order_id){
        $orderDetails = Order::where('id', $order_id)->first()->toArray();

        $orderDetails['order_id'] = $orderDetails['id'];
        $orderDetails['order_date'] = $orderDetails['created_at'];
        $orderDetails['pickup_location'] = "LuminoSoft Office";
        $orderDetails['channel_id'] = "2394849";
        $orderDetails['comment'] = "Luminosoft Office Order";
        $orderDetails['reseller_name'] = "";
        $orderDetails['company_name'] = "Luminosoft";
        $orderDetails['billing_customer_name'] = $orderDetails['customer_name'];
        $orderDetails['billing_last_name'] = "";
        $orderDetails['billing_address'] = $orderDetails['customer_address'];
        $orderDetails['billing_address_2'] = "";
        $orderDetails['billing_city'] = $orderDetails['customer_city'];
        $orderDetails['billing_pincode'] = $orderDetails['customer_zip'];
        $orderDetails['billing_state'] = $orderDetails['customer_city'];
        $orderDetails['billing_country'] = $orderDetails['customer_country'];
        $orderDetails['billing_email'] = $orderDetails['customer_email'];
        $orderDetails['billing_phone'] = $orderDetails['customer_phone'];
        $orderDetails['shipping_is_billing'] = true;
        $orderDetails['shipping_customer_name'] = $orderDetails['customer_name'];
        $orderDetails['shipping_last_name'] = "";
        $orderDetails['shipping_address'] = $orderDetails['customer_address'];
        $orderDetails['shipping_address_2'] = "";
        $orderDetails['shipping_city'] = $orderDetails['customer_city'];
        $orderDetails['shipping_pincode'] = $orderDetails['customer_zip'];
        $orderDetails['shipping_state'] = $orderDetails['customer_city'];
        $orderDetails['shipping_country'] = $orderDetails['customer_country'];
        $orderDetails['shipping_email'] = $orderDetails['customer_email'];
        $orderDetails['shipping_phone'] = $orderDetails['customer_phone'];

        $cart = json_decode($orderDetails['cart'],true);

        $orderDetails['order_items'] = $cart['items'];
        foreach($orderDetails['order_items'] as $key=> $items){

            $orderDetails['order_items'][$key]['name'] = $items['item']['name'];
            $orderDetails['order_items'][$key]['sku'] = $items['item']['id'];
            $orderDetails['order_items'][$key]['units'] = $items['qty'];
            $orderDetails['order_items'][$key]['selling_price'] = $items['price'];
            $orderDetails['order_items'][$key]['discount'] = "";
            $orderDetails['order_items'][$key]['tax'] = "";
            $orderDetails['order_items'][$key]['hsn'] = "";

        }

        $orderDetails['payment_method'] = $orderDetails['method'];
        $orderDetails['shipping_charges'] = $orderDetails['shipping_cost'];
        $orderDetails['giftwrap_charges'] = $orderDetails['packing_cost'];
        $orderDetails['transaction_charges'] = "";
        $orderDetails['total_discount'] = $orderDetails['coupon_discount'];
        $orderDetails['sub_total'] = $orderDetails['pay_amount'];
        $orderDetails['length'] = $orderDetails['length'];
        $orderDetails['breadth'] = $orderDetails['breadth'];
        $orderDetails['height'] = $orderDetails['height'];
        $orderDetails['weight'] = $orderDetails['weight'];
        $orderDetails['ewaybill_no'] = "";
        $orderDetails['customer_gstin'] = "";

        // echo "<pre>"; print_r(json_encode($orderDetails)); die;

        $orderDetails = json_encode($orderDetails);

        // Get Access Token
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);
        // echo "<prep>"; print_r($server_output); die;

        // Crete Order in Ship Rocket through Code
        $url = "https://apiv2.shiprocket.in/v1/external/orders/update/adhoc";
        $c = curl_init($url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,$orderDetails);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);


        // $result = json_decode($result,true);
        print_r($result); die;


    }


}
