<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Cast\Array_;

class APIController Extends Controller
{
    public function pushOrder($id)
    {
        $getResult = Order::pushOrder($id);
        return response()->json(['status'=>$getResult]);
    }

    public function createReturnOrders($order_id){


        $orderDetails = Order::where('order_id', $order_id)->first()->toArray();

        $orderDetails['order_id'] = $orderDetails['order_id'];
        $orderDetails['order_date'] = $orderDetails['created_at'];
        // $orderDetails['pickup_location'] = "LuminoSoft Office";
        $orderDetails['channel_id'] = "2394849";
        // $orderDetails['comment'] = "Luminosoft Office Order";
        // $orderDetails['reseller_name'] = "";

        $orderDetails['pickup_customer_name'] = $orderDetails['customer_name'];
        $orderDetails['pickup_last_name'] = "";
        $orderDetails['company_name'] = "Luminosoft";
        $orderDetails['pickup_address'] = $orderDetails['customer_address'];
        $orderDetails['pickup_address_2'] = "";
        $orderDetails['pickup_city'] = $orderDetails['customer_city'];
        //
        $orderDetails['pickup_state'] = $orderDetails['customer_city'];
        $orderDetails['pickup_country'] = $orderDetails['customer_country'];
        $orderDetails['pickup_email'] = $orderDetails['customer_email'];
        $orderDetails['pickup_phone'] = $orderDetails['customer_phone'];
        $orderDetails['pickup_isd_code'] = "+91";
        // $orderDetails['shipping_is_billing'] = true;
        $orderDetails['shipping_customer_name'] = $orderDetails['customer_name'];
        $orderDetails['shipping_last_name'] = "";
        $orderDetails['shipping_address'] = $orderDetails['customer_address'];
        $orderDetails['shipping_address_2'] = "";
        $orderDetails['shipping_city'] = $orderDetails['customer_city'];
        $orderDetails['shipping_country'] = $orderDetails['customer_country'];
        $orderDetails['shipping_pincode'] = $orderDetails['customer_zip'];
        $orderDetails['shipping_state'] = $orderDetails['customer_city'];
        $orderDetails['shipping_email'] = $orderDetails['customer_email'];
        $orderDetails['shipping_isd_code'] = "+91";
        $orderDetails['shipping_phone'] = $orderDetails['customer_phone'];



        $cart = json_decode($orderDetails['cart'],true);

        $orderDetails['order_items'] = $cart['items'];
        foreach($orderDetails['order_items'] as $key=> $items){


            $orderDetails['order_items'][$key]['sku'] = $items['item']['id'];
            $orderDetails['order_items'][$key]['name'] = $items['item']['name'];
            $orderDetails['order_items'][$key]['units'] = $items['qty'];
            $orderDetails['order_items'][$key]['selling_price'] = $items['price'];
            $orderDetails['order_items'][$key]['discount'] = "";
            $orderDetails['order_items'][$key]['brand'] = "";
            $orderDetails['order_items'][$key]['hsn'] = "";
            $orderDetails['order_items'][$key]['qc_enable'] = true;
            $orderDetails['order_items'][$key]['qc_size'] = "";

        }

        $orderDetails['payment_method'] = $orderDetails['method'];
        // $orderDetails['shipping_charges'] = $orderDetails['shipping_cost'];
        // $orderDetails['giftwrap_charges'] = $orderDetails['packing_cost'];
        // $orderDetails['transaction_charges'] = "";
        $orderDetails['total_discount'] = $orderDetails['coupon_discount'];
        $orderDetails['sub_total'] = $orderDetails['pay_amount'];
        $orderDetails['length'] = $orderDetails['length'];
        $orderDetails['breadth'] = $orderDetails['breadth'];
        $orderDetails['height'] = $orderDetails['height'];
        $orderDetails['weight'] = $orderDetails['weight'];

        // echo "<pre>"; print_r(json_encode($orderDetails)); die;

        $orderDetails = json_encode($orderDetails);

        // Get Access Token
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);
        // echo "<prep>"; print_r($server_output); die;

        // Crete Order in Ship Rocket through Code
        $url = "https://apiv2.shiprocket.in/v1/external/orders/create/return";
        $c = curl_init($url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,$orderDetails);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $result = json_decode($result,true);
        print_r($result); die();

        // if(isset($result['status_code'])&&$result['status_code']==1)
        // {
        //     Order::where('id', $order_id)->update(['order_id'=>$result['order_id'],'shipment_id'=>$result['shipment_id']]);
        //     // print_r($result);
        //     return redirect()->route('ship-order-details');
        // }
        // else
        // {
        //     return "There is an Issue when pushing order to shiprocket! Please Contcat with Admin";
        // }
    }

    public function getOrder(){
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);

        $url = "https://apiv2.shiprocket.in/v1/external/orders";
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_POST,0);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $results = json_decode($result,true);

        //  dd($results);die();

        return view('admin.shiprocket.index', ["results"=>$results]);

    }

    public function getSingleOrder($order_id){
        $orderDetails = Order::where('order_id', $order_id)->first();
        $number = $orderDetails['order_id'];
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);

        $url = "https://apiv2.shiprocket.in/v1/external/orders/show/".$number;
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_POST,0);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $results = json_decode($result,true);

        // print_r($results); die();

        // return redirect()->route('ship-order-details');
        $data = $results['data'];
        // dd($data);die();
        return view('admin.shiprocket.order_details', ["results"=>$data]);
    }

    public function generateAwb($shipment_id)
    {
        $orderDetails = Order::where('shipment_id', $shipment_id)->first()->toArray();

        // $order = $orderDetails['shipment_id'];
        $order =  $orderDetails['shipment_id'];
        // $orderDetails['courier_id'] =  "";
        // $orderDetails['status'] =  "";

        // $orderDetails = json_encode($orderDetails);
        // print_r($order);die();
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);
        // echo "<prep>"; print_r($server_output); die;

        // Crete Order in Ship Rocket through Code
        $url = "https://apiv2.shiprocket.in/v1/external/courier/assign/awb";
        $c = curl_init($url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,'{
            "shipment_id": "'.$order.'"

        }');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $result = json_decode($result,true);

        // if(isset($result['awb_assign_status']) == 0)
        // {
        //     // $url = $result['label_url'];

        //     return view('admin.shiprocket.label');
        // }
        // print_r($result); die();

        // $url = $result['label_url'];

        // foreach($result['data'] as $data){
        //     $ship = $data['shipments'];
        // }

        // print_r($result);die();
        return redirect()->route('generate-label',$order);
    }

    public function readyToShipOrder()
    {
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);

        $url = "https://apiv2.shiprocket.in/v1/external/orders";
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_POST,0);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $results = json_decode($result,true);

        // dd($result);die();

        return view('admin.shiprocket.ready_to_ship', ["results"=>$results]);
    }

    public function pickupOrders()
    {
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);

        $url = "https://apiv2.shiprocket.in/v1/external/orders";
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_POST,0);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $results = json_decode($result,true);

        return view('admin.shiprocket.pickup', ["results"=>$results]);
    }

    public function cancelOrder($order_id)
    {
        $orderDetails = Order::where('order_id', $order_id)->first()->toArray();

        $order = $orderDetails['order_id'];
        $orderDetails['ids'] =  [$order];

        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);
        // echo "<prep>"; print_r($server_output); die;

        // Crete Order in Ship Rocket through Code
        $url = "https://apiv2.shiprocket.in/v1/external/orders/cancel";
        $c = curl_init($url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS ,'{
            "ids": ["'.$order.'"]

        }');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $result = json_decode($result,true);

        return redirect()->route('ship-order-details');

    }

    public function updateOrder($order_id){
        $orderDetails = Order::where('id', $order_id)->first()->toArray();

        $orderDetails['order_id'] = $orderDetails['order_id'];
        $orderDetails['order_date'] = $orderDetails['created_at'];
        $orderDetails['pickup_location'] = "LuminoSoft Office";
        $orderDetails['channel_id'] = "2394849";
        $orderDetails['comment'] = "Luminosoft Office Order";
        $orderDetails['reseller_name'] = "";
        $orderDetails['company_name'] = "Luminosoft";
        $orderDetails['billing_customer_name'] = $orderDetails['customer_name'];
        $orderDetails['billing_last_name'] = "";
        $orderDetails['billing_address'] = $orderDetails['customer_address'];
        $orderDetails['billing_address_2'] = "";
        $orderDetails['billing_city'] = $orderDetails['customer_city'];
        $orderDetails['billing_pincode'] = $orderDetails['customer_zip'];
        $orderDetails['billing_state'] = $orderDetails['customer_city'];
        $orderDetails['billing_country'] = $orderDetails['customer_country'];
        $orderDetails['billing_email'] = $orderDetails['customer_email'];
        $orderDetails['billing_phone'] = $orderDetails['customer_phone'];
        $orderDetails['shipping_is_billing'] = true;
        $orderDetails['shipping_customer_name'] = $orderDetails['customer_name'];
        $orderDetails['shipping_last_name'] = "";
        $orderDetails['shipping_address'] = $orderDetails['customer_address'];
        $orderDetails['shipping_address_2'] = "";
        $orderDetails['shipping_city'] = $orderDetails['customer_city'];
        $orderDetails['shipping_pincode'] = $orderDetails['customer_zip'];
        $orderDetails['shipping_state'] = $orderDetails['customer_city'];
        $orderDetails['shipping_country'] = $orderDetails['customer_country'];
        $orderDetails['shipping_email'] = $orderDetails['customer_email'];
        $orderDetails['shipping_phone'] = $orderDetails['customer_phone'];

        $cart = json_decode($orderDetails['cart'],true);

        $orderDetails['order_items'] = $cart['items'];
        foreach($orderDetails['order_items'] as $key=> $items){

            $orderDetails['order_items'][$key]['name'] = $items['item']['name'];
            $orderDetails['order_items'][$key]['sku'] = $items['item']['id'];
            $orderDetails['order_items'][$key]['units'] = $items['qty'];
            $orderDetails['order_items'][$key]['selling_price'] = $items['price'];
            $orderDetails['order_items'][$key]['discount'] = "";
            $orderDetails['order_items'][$key]['tax'] = "";
            $orderDetails['order_items'][$key]['hsn'] = "";

        }

        $orderDetails['payment_method'] = $orderDetails['method'];
        $orderDetails['shipping_charges'] = $orderDetails['shipping_cost'];
        $orderDetails['giftwrap_charges'] = $orderDetails['packing_cost'];
        $orderDetails['transaction_charges'] = "";
        $orderDetails['total_discount'] = $orderDetails['coupon_discount'];
        $orderDetails['sub_total'] = $orderDetails['pay_amount'];
        $orderDetails['length'] = $orderDetails['length'];
        $orderDetails['breadth'] = $orderDetails['breadth'];
        $orderDetails['height'] = $orderDetails['height'];
        $orderDetails['weight'] = $orderDetails['weight'];
        $orderDetails['ewaybill_no'] = "";
        $orderDetails['customer_gstin'] = "";

        // echo "<pre>"; print_r(json_encode($orderDetails)); die;

        $orderDetails = json_encode($orderDetails);

        // Get Access Token
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);
        // echo "<prep>"; print_r($server_output); die;

        // Crete Order in Ship Rocket through Code
        $url = "https://apiv2.shiprocket.in/v1/external/orders/update/adhoc";
        $c = curl_init($url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,$orderDetails);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);


        // $result = json_decode($result,true);
        print_r($result); die;
    }



    public function exportOrders()
    {
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);
        // echo "<prep>"; print_r($server_output); die;

        // Crete Order in Ship Rocket through Code
        $url = "https://apiv2.shiprocket.in/v1/external/orders/export";
        $c = curl_init($url);
        curl_setopt($c,CURLOPT_POST,1);
        // curl_setopt($c,CURLOPT_POSTFIELDS,$orderDetails);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);


        dd($result);die();


        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        // CURLOPT_URL => 'https://apiv2.shiprocket.in/v1/external/orders/export',
        // CURLOPT_RETURNTRANSFER => true,
        // CURLOPT_ENCODING => '',
        // CURLOPT_MAXREDIRS => 10,
        // CURLOPT_TIMEOUT => 0,
        // CURLOPT_FOLLOWLOCATION => true,
        // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        // CURLOPT_CUSTOMREQUEST => 'POST',
        // CURLOPT_POSTFIELDS =>'{

        // }',
        // CURLOPT_HTTPHEADER => array(
        //     'Content-Type: application/json',
        //     'Authorization: Bearer {{'.$server_output['token'].'}}'
        // ),
        // ));

        // $response = curl_exec($curl);

        // curl_close($curl);
        // echo $response;

    }

    public function getChannel(){
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);

        $url = "https://apiv2.shiprocket.in/v1/external/channels";
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_POST,0);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $results = json_decode($result,true);

        return view('admin.shiprocket.channel', ["results"=>$results]);
    }

    public function getCurier($order_id)
    {
        $orderDetails = Order::where('order_id', $order_id)->first();
        $number = $orderDetails['order_id'];
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);

        $url = "https://apiv2.shiprocket.in/v1/external/courier/serviceability?order_id=".$number;
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_POST,0);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $results = json_decode($result,true);
        $curier = $results['data'];
        // $curiers = $curier['available_courier_companies'];
        //  dd($curier);
        return view('admin.shiprocket.curier', ["results"=>$curier]);

    }

    public function generateInvoice($order_id)
    {
        $orderDetails = Order::where('order_id', $order_id)->first()->toArray();
        // print_r($orderDetails['order_id']);die();
        // $test_id = array();
        //  $test_id = [190401551];
        // $orderDetails2['ids'] = $orderDetails['order_id'];
        // $test_id = $orderDetails2['ids'];
        // print_r($test_id);die();

        // $orderDetails3 = json_decode($test_id,true);
        // $example = explode(" ",$orderDetails3);
        // var_dump($example);die();
        // $test_1 = array("ids"=>$test_id);
        // $orderDetails4 = json_encode($test_1,true);
        // print_r($orderDetails4); die();
        // $data = http_build_query($test_1);
        $order = $orderDetails['order_id'];
        $orderDetails['ids'] =  [$order];

        // foreach($orderDetails['ids'] as $item){
        //     print_r($item);die();
        //     $orderDetails['ids'] = $item['order_id'];

        // }
        // $orderDetails = [$orderDetails];

        // print_r($orderDetails[0]['order_id']); die();
        // $data = ;
        // Get Access Token
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);
        // echo "<prep>"; print_r($server_output); die;

        // Crete Order in Ship Rocket through Code
        $url = "https://apiv2.shiprocket.in/v1/external/orders/print/invoice";
        $c = curl_init($url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS ,'{
            "ids": ["'.$order.'"]

        }');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $result = json_decode($result,true);

        $url = $result['invoice_url'];

        return view('admin.shiprocket.invoice', ["results"=>$url]);

    }

    public function generateLabel($shipment_id)
    {
        $orderDetails = Order::where('shipment_id', $shipment_id)->first()->toArray();

        $order = $orderDetails['shipment_id'];
        $orderDetails['shipment_id'] =  [$order];
        // print_r($order);die();
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);
        // echo "<prep>"; print_r($server_output); die;

        // Crete Order in Ship Rocket through Code
        $url = "https://apiv2.shiprocket.in/v1/external/courier/generate/label";
        $c = curl_init($url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS ,'{
            "shipment_id": ["'.$order.'"]

        }');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $result = json_decode($result,true);

        if(isset($result['label_created']) == 1)
        {
            $url = $result['label_url'];

            return view('admin.shiprocket.label', ["results"=>$url]);
        }
        elseif(isset($result['label_created']) == 0)
        {
            $response = $result['response'];
            print_r($response); die();
            // return view('admin.shiprocket.label', ["response"=>$response]);
        }
        else{
            return "Please Contact to Support";
        }


    }

    public function pickupRequest($shipment_id)
    {
        $orderDetails = Order::where('shipment_id', $shipment_id)->first()->toArray();

        $order = $orderDetails['shipment_id'];
        $order_id = $orderDetails['order_id'];
        $orderDetails['shipment_id'] =  [$order];
        // print_r($order);die();
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);
        // echo "<prep>"; print_r($server_output); die;

        // Crete Order in Ship Rocket through Code
        $url = "https://apiv2.shiprocket.in/v1/external/courier/generate/pickup";
        $c = curl_init($url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS ,'{
            "shipment_id": ["'.$order.'"]

        }');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $result = json_decode($result,true);

        // dd($result);die();

        // $url = $result['label_url'];

        return redirect()->route('generate-manifest',$order);

    }

//     array:2 [▼
//   "pickup_status" => 1
//   "response" => array:6 [▼
//     "pickup_scheduled_date" => "2022-03-07 11:43:24"
//     "pickup_token_number" => "Reference No: 194_BIGFOOT 2088932_08032022"
//     "status" => 3
//     "others" => "{"tier_id":1,"etd_zone":"z_a","etd_hours":"{\"assign_to_pick\":8,\"pick_to_ship\":21.800000000000001,\"ship_to_deliver\":35.700000000000003,\"etd_zone\":\"z_a\" ▶"
//     "pickup_generated_date" => array:3 [▼
//       "date" => "2022-03-07 11:43:24.578823"
//       "timezone_type" => 3
//       "timezone" => "Asia/Kolkata"
//     ]
//     "data" => "Pickup is confirmed by Xpressbees 1kg For AWB :- 143254214169980"
//   ]
// ]

    public function generateManifest($shipment_id)
    {
        $orderDetails = Order::where('shipment_id', $shipment_id)->first()->toArray();

        $order = $orderDetails['shipment_id'];
        $order_id = $orderDetails['order_id'];
        $orderDetails['shipment_id'] =  [$order];
        // print_r($order);die();
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);
        // echo "<prep>"; print_r($server_output); die;

        // Crete Order in Ship Rocket through Code
        $url = "https://apiv2.shiprocket.in/v1/external/manifests/generate";
        $c = curl_init($url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS ,'{
            "shipment_id": ["'.$order.'"]

        }');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $result = json_decode($result,true);

        // dd($result);die();

        // if(isset($result['status_code']) == 400){
        //     return $result['message'];
        // }
        if(isset($result['message']) == "Manifest already generated."){
            return redirect()->route('download-manifest',$order_id);
        }
        elseif(isset($result['status']) == 1)
        {
            $url = $result['manifest_url'];

            return view('admin.shiprocket.manifest', ["results"=>$url]);
        }
        else{
            return $result;
        }

    }

    public function downloadManifest($order_id)
    {
        $orderDetails = Order::where('order_id', $order_id)->first()->toArray();

        $order = $orderDetails['order_id'];
        $orderDetails['order_id'] =  [$order];
        // print_r($order);die();
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);
        // echo "<prep>"; print_r($server_output); die;

        // Crete Order in Ship Rocket through Code
        $url = "https://apiv2.shiprocket.in/v1/external/manifests/print";
        $c = curl_init($url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS ,'{
            "order_ids": ["'.$order.'"]

        }');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $result = json_decode($result,true);

        // dd($result);die();

        if(isset($result['status_code']) == 400){
            return $result['message'];
        }else{
            $url = $result['manifest_url'];

            return view('admin.shiprocket.manifest', ["results"=>$url]);
        }



    }


    public function generateTrack($shipment_id)
    {
        $orderDetails = Order::where('shipment_id', $shipment_id)->first();
        $number = $orderDetails['shipment_id'];
        $c = curl_init();
        $url = "https://apiv2.shiprocket.in/v1/external/auth/login";
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c,CURLOPT_POST,1);
        curl_setopt($c,CURLOPT_POSTFIELDS,"email=tareqhossain361@gmail.com&password=Tareq1245");
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec($c);
        curl_close($c);
        $server_output = json_decode($server_output,true);

        $url = "https://apiv2.shiprocket.in/v1/external/courier/track/shipment/".$number;
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_POST,0);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($c,CURLOPT_)
        curl_setopt($c,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Authorization:Bearer '.$server_output['token'].''));
        $result = curl_exec($c);
        curl_close($c);

        $results = json_decode($result,true);
        $track = $results['tracking_data'];
        $tracks = $track['track_url'];
        // dd($curier); die();
        return view('admin.shiprocket.tracking', ["results"=>$tracks]);
    }



}
